<?php

/**
 * @file
 * Documents hooks provided by the Commerce Paybox module.
 */

/**
 * Allow other modules to alter Paybox CGI module parameters.
 *
 * For a full list of allowed parameters, see ocumentation at
 * http://www1.paybox.com/telechargement_focus.aspx?cat=3
 *
 * @param array $params
 *   The Paybox CGI module parameters to be altered.
 * @param object $order
 *   A fully loaded order object.
 * @param array $settings
 *   The commerce payment method (paybox_offsite) settings.
 */
function hook_commerce_paybox_offsite_params_alter(array &$params, $order, array $settings) {
  // The paybox site will allow only credit cards payment methods.
  $params['PBX_TYPEPAIEMENT'] = 'CARTE';
  $params['PBX_TYPECARTE'] = 'CB';
}

/**
 * Allow other modules to alter 3DSv2 parameters.
 *
 * See the structure of $params in commerce_paybox_add_3dsv2_params().
 *
 * @param array $params
 * @param \EntityDrupalWrapper $order_wrapper
 * @return void
 */
function hook_commerce_paybox_3dsv2_params_alter(array &$params, $order_wrapper) {
  $params['billing']['country'] = 'FR';
}
