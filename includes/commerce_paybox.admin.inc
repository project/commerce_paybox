<?php

/**
 * @file
 * Admin page callbacks for the Commerce Paybox module.
 */

/**
 * Builds and returns the Paybox settings form.
 *
 * @see commerce_paybox_settings_validate()
 *
 * @ingroup forms
 */
function commerce_paybox_settings($form, &$form_state) {
  $rule_commerce_payment_paybox = rules_config_load('commerce_payment_paybox_offsite');
  $cgi_module_path = variable_get('commerce_paybox_cgi_module_path', '');
  if ($rule_commerce_payment_paybox->active) {
    $bin_path = DRUPAL_ROOT . '/' . $cgi_module_path;
    if (is_file($bin_path) && !is_executable($bin_path)) {
      $message = t('File "@path" is not executable. Please, check permissions on this file.', array('@path' => $cgi_module_path));
      drupal_set_message($message, 'error');
    }
  }
  else {
    $form['notice'] = array(
      '#type' => 'item',
      '#title' => t('Notice'),
      '#markup' => t('None of this settings will be used unless you activate the "Paybox System" payment method.'),
    );
  }

  $form['commerce_paybox_hmac_authentication'] = array(
    '#type' => 'fieldset',
    '#title' => t('HMAC authentication'),
    'commerce_paybox_hmac_key' => array(
      '#type' => 'textfield',
      '#size' => '128',
      '#title' => t('Paybox private key'),
      '#description' => t("Private key generated on paybox's admin interface"),
      '#default_value' => variable_get('commerce_paybox_hmac_key', ''),
    ),
    'commerce_paybox_hmac_alg' => array(
      '#type' => 'select',
      '#title' => t('Algorithm to use with HMAC method'),
      '#description' => t('Select algorithm from allowed list (SHA512 by default)'),
      '#options' => array(
        'SHA512' => 'SHA512',
        'SHA256' => 'SHA256',
        'SHA384' => 'SHA384',
        'RIPEMD160' => 'RIPEMD160',
        'SHA224' => 'SHA224',
        'MDC2' => 'MDC2',
      ),
      '#default' => variable_get('commerce_paybox_hmac_alg', 'SHA512'),
    ),
  );

  $form['commerce_paybox_cgi_module'] = array(
    '#type' => 'fieldset',
    '#title' => t('CGI module (deprecated)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'commerce_paybox_cgi_module_path' => array(
      '#type' => 'textfield',
      '#title' => t('Path to the Paybox System CGI module (if not using HMAC key, which is recommended)'),
      '#description' => t('Path to the Paybox CGI module, relative to the drupal root filesystem (ex : sites/all/libraries/paybox/module_debian64bits_noyau2-6-18_libc2-3-6.cgi).'),
      '#default_value' => $cgi_module_path,
    ),
  );

  $form['commerce_paybox_3dsv2'] = array(
    '#type' => 'fieldset',
    '#title' => t('3DS v2 (PSD2 without systematic challenge)'),
    '#collapsed' => TRUE,
    'commerce_paybox_3dsv2_enabled' => array(
      '#type' => 'checkbox',
      '#title' => t('Enable 3DS v2'),
      '#default_value' => variable_get('commerce_paybox_3dsv2_enabled'),
    ),
    'commerce_paybox_product_line_item_types' => array(
      '#type' => 'checkboxes',
      '#options' => commerce_line_item_type_get_name(),
      '#title' => t('Product line item types'),
      '#description' => t('Line item types which are used to calculate the total shipping cart quantity.'),
      '#default_value' => variable_get('commerce_paybox_product_line_item_types'),
    ),
  );

  $form['commerce_paybox_pubkey_path'] = array(
    '#type' => 'textfield',
    '#required' => $rule_commerce_payment_paybox->active,
    '#title' => t('Path to the Paybox System public key file'),
    '#description' => t('Path to the Paybox public key file, relative to the drupal root filesystem (ex : sites/all/libraries/paybox/pubkey.pem).'),
    '#default_value' => variable_get('commerce_paybox_pubkey_path', ''),
  );
  $form['commerce_paybox_use_order_number'] = array(
    '#type' => 'checkbox',
    '#title' => t('User order_number instead of transaction_id for PBX_CMD'),
    '#description' => t('PBX_CMD requires a unique value. By default, <code>transaction_id</code> is used to ensure uniqueness. However, <code>order_number</code> can also be used so that Paybox back-office could be more helpful. If PBX_RETOUR is modified, please ensure that it contains <code>order:R</code> (when transaction_id is used: <code>txnid:R</code>).'),
    '#default_value' => variable_get('commerce_paybox_use_order_number', FALSE),
  );

  $form['commerce_paybox_domain'] = array(
    '#type' => 'select',
    '#title' => t('Paybox payment domain'),
    '#options' => array(
      'paybox.com' => t('Paybox'),
      'e-transactions.fr' => t('Up2pay E-Transactions'),
    ),
    '#description' => t('Choose Paybox unless you use a white label service.'),
    '#default_value' => variable_get('commerce_paybox_domain', 'paybox.com'),
  );

  $paybox_servers = variable_get('commerce_paybox_paybox_servers', array());
  $form['commerce_paybox_paybox_servers'] = array(
    '#type' => 'textarea',
    '#required' => $rule_commerce_payment_paybox->active,
    '#title' => t('Paybox IP addresses'),
    '#description' => t('Paybox payment servers IP addresses (one per line)'),
    '#default_value' => implode("\n", $paybox_servers),
  );

  return system_settings_form($form);
}

/**
 * Validates the Paybox settings form.
 */
function commerce_paybox_settings_validate($form, &$form_state) {
  $form_state['values']['commerce_paybox_cgi_module_path'] = trim($form_state['values']['commerce_paybox_cgi_module_path'], '/');

  $hmac_key = $form_state['values']['commerce_paybox_hmac_key'];
  if ($hmac_key == '') {
    $cgi_module_path = $form_state['values']['commerce_paybox_cgi_module_path'];
    if (!is_file(DRUPAL_ROOT . '/' . $cgi_module_path)) {
      $message = t('No HMAC key defined, which is the preferred method for paybox');
      form_set_error('commerce_paybox_hmac_key', $message);
      $message = t('File "@path" could not be found on the server.', array('@path' => $cgi_module_path));
      form_set_error('commerce_paybox_cgi_module_path', $message);
    }
  }

  $pubkey_path = $form_state['values']['commerce_paybox_pubkey_path'];
  if (!is_file(DRUPAL_ROOT . '/' . $pubkey_path)) {
    $message = t('File "@path" could not be found on the server.', array('@path' => $pubkey_path));
    form_set_error('commerce_paybox_pubkey_path', $message);
  }

  $form_state['values']['commerce_paybox_paybox_servers'] = array_map('trim', explode("\n", $form_state['values']['commerce_paybox_paybox_servers']));
  $form_state['values']['commerce_paybox_product_line_item_types'] = array_values(array_filter($form_state['values']['commerce_paybox_product_line_item_types']));
}
