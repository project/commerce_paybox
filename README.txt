DESCRIPTION
===========
Paybox by Verifone (https://www.paybox.com/) integration for the Drupal Commerce
payment and checkout system. It supports Paybox and Crédit Agricole Up2pay
E-Transactions system.

INSTALLATION INSTRUCTIONS FOR COMMERCE PAYBOX
=============================================

First you should make sure you have an Paybox Merchant account, and are ready
to configure it. There are two ways to work with Paybox: CGI module and HMAC
authentication.

More informations about Paybox integration could be found at Paybox website
(https://www.paybox.com/espace-integrateur-documentation/manuels/?lang=en).

CGI module (deprecated)
-----------------------
This method is deprecated and does not support newer requirements. No more new
install is supported. This module supports this method only for compatibility
with current installs.

HMAC authentication
-------------------
 - Prepare different informations fields provided by Verifone when the
   registration of the Marchant is confirmed: site number (7 digits), rank
   number (3 digits) and identifier (1 to 9 digits).
 - Generate a HMAC private key from the Merchant Back Office.
 - Install the Paxbox module.
 - Navigate to the admin section of the Paybox module (Store > Configuration >
   Paybox System) and set the HMAC private key and other options.
 - Enable the Paybox payment rule (Store > Configuration > Payment methods).
 - Edit the Paybox payment rule "Action settings" with your paybox informations
   (be sure to uncheck "Use Paybox test plateform" on production).

3DSv2
-----
The Second Payment Services Directive (PSD2) coming into effect imposes the
authentication of the cardholder for all e-commerce payments before April 2021.

A new authentication protocol, defined by the Carte Bancaire group, Visa and
Mastercard allows to obtain this authentication without a systematic challenge
of the cardholder, usually done by sending an SMS or using an official banking
application. This new protocol, 3DSv2, requires the merchant to consult its bank
in order to obtain a ‘VADS’ contract. This contract will need to be transmitted
to Verifone, who will then proceed with the enrollment and the configuration of
said contract.

Then it could be configured in the admin section of the Paybox module.

Notes
-----
Paybox module requires the Paybox servers to be able to contact your
Drupal site for orders validation with an IPN. During the test phase, if your
web server is protected from external connection, make sure that it accepts
requests from the Paybox sandbox servers.

In other ways, you'll have to allow your IP (usually 127.0.0.1) to validate
orders (Store > Configuration > Paybox System), and the call the Paybox IPN
yourself.
